*** Life<FredkinCell> 9x2 ***

Generation = 0, Population = 11.
00
-0
-0
--
00
--
0-
00
00

Generation = 1, Population = 9.
1-
--
01
0-
11
-0
1-
1-
--

Generation = 2, Population = 10.
-0
-0
-2
-0
--
01
2-
20
0-

Generation = 3, Population = 12.
11
0-
0-
01
1-
-2
-0
31
1-

*** Life<FredkinCell> 7x5 ***

Generation = 0, Population = 15.
-0--0
-00-0
000--
---0-
00---
00---
-00--

Generation = 1, Population = 15.
01-01
-1--1
11--0
----0
--00-
-1---
--10-

Generation = 2, Population = 18.
1--1-
0-0--
2-00-
000-1
--11-
0-0--
--210

*** Life<FredkinCell> 8x6 ***

Generation = 0, Population = 25.
0--0-0
0-00--
0-000-
00-000
0--00-
00--0-
-----0
0-0--0

Generation = 1, Population = 23.
10-1--
---1-0
-0----
1101-1
-00-1-
-10-1-
-00--1
---001

Generation = 2, Population = 26.
21-200
0-02--
-1----
2-120-
-1102-
021-2-
0---02
-0-1--

Generation = 3, Population = 22.
-20--1
1-1--0
0-0-0-
---3-1
0--130
-3-0--
100--3
-----1

*** Life<FredkinCell> 6x5 ***

Generation = 0, Population = 16.
0--00
-000-
0--00
0-00-
--0--
-0-00

Generation = 1, Population = 18.
----1
01-10
1-011
1----
0-100
0-011

Generation = 2, Population = 13.
00--2
-20-1
-012-
-0---
---1-
1--2-

Generation = 3, Population = 21.
1--03
-3-12
1123-
11---
00120
-0031

Generation = 4, Population = 13.
--0--
0402-
-----
220-0
--23-
--14-

Generation = 5, Population = 14.
1-1--
1-132
---31
3--0-
0-3--
-0--1

*** Life<FredkinCell> 3x1 ***

Generation = 0, Population = 2.
-
0
0

Generation = 1, Population = 3.
0
1
1

Generation = 2, Population = 2.
1
-
2

Generation = 3, Population = 0.
-
-
-

Generation = 4, Population = 0.
-
-
-

*** Life<FredkinCell> 1x5 ***

Generation = 0, Population = 2.
--0-0

Generation = 1, Population = 1.
-0---

Generation = 2, Population = 2.
0-0--

Generation = 3, Population = 1.
---0-

Generation = 4, Population = 2.
--0-0

Generation = 5, Population = 1.
-0---

Generation = 6, Population = 2.
0-0--

Generation = 7, Population = 1.
---0-

Generation = 8, Population = 2.
--0-0

*** Life<FredkinCell> 1x8 ***

Generation = 0, Population = 2.
00------

Generation = 1, Population = 3.
110-----

Generation = 2, Population = 3.
2-10----

*** Life<FredkinCell> 10x3 ***

Generation = 0, Population = 14.
--0
0--
000
0--
0-0
--0
---
0-0
-0-
0-0

Generation = 1, Population = 15.
00-
1--
1-1
---
1-1
001
0--
-0-
0-0
-0-

Generation = 2, Population = 12.
-10
--0
2--
---
202
1--
100
0--
---
---

Generation = 3, Population = 15.
02-
1-1
-01
-00
--3
-0-
2-1
1-0
0--
---

Generation = 4, Population = 17.
-3-
2-2
--2
0-1
-04
--1
302
--1
100
0--

Generation = 5, Population = 13.
---
-03
-0-
-0-
-15
10-
4-3
-0-
---
1-0

Generation = 6, Population = 13.
-30
2-4
2--
0--
-26
--1
5--
---
100
---

Generation = 7, Population = 20.
-4-
305
-02
1-1
-3-
1-2
-03
-01
2-1
100

Generation = 8, Population = 11.
-5-
4--
2-3
--2
2-6
--3
--4
--2
3--
---

*** Life<FredkinCell> 9x10 ***

Generation = 0, Population = 52.
-000---000
0--0-00-0-
000--00-00
-0-00-00-0
00--0-00-0
--0--0000-
---000---0
000000--00
000-0----0

Generation = 1, Population = 55.
-1--00-111
1001---0-0
-110--10--
0-01-01-0-
1---101101
0---0-1110
000-11-001
-111--0011
-1-01----1

Generation = 2, Population = 44.
-2--11----
2-1--0--01
0--10----0
10--0-2-1-
-00--1-2--
--00-----1
-1-0-2-1-2
-22-0-1122
020-2000-2

Generation = 3, Population = 41.
---02-0-11
30--01001-
---2--1-01
----10-0--
------1--1
-0--0---12
02-11-020-
-3-11----3
13----1-03

Generation = 4, Population = 45.
0201-111-2
4111----21
011-002--2
----21----
-0-------2
-10-10----
13--2-1--2
0----0-124
2-0020--1-

Generation = 5, Population = 38.
-31--2-213
5-------32
-2-2-130-3
1-01------
11------0-
-21--1----
---13----3
--2111123-
3-1---1-2-

Generation = 6, Population = 52.
-4212--32-
-111--00-3
0-13-2-104
2-1-212010
--00-1-2-2
--2---1-12
-30--212--
03-22-2---
--2-2-2033

Generation = 7, Population = 45.
0-3232--3-
---2--11-4
122-03-215
3---3---21
111112-3--
-----12-2-
-----3-3-3
1--3-1-234
3--03----4

*** Life<FredkinCell> 9x3 ***

Generation = 0, Population = 14.
00-
-0-
0-0
00-
-0-
--0
--0
-00
00-

Generation = 1, Population = 8.
1-0
01-
1--
---
-1-
--1
---
---
1--

Generation = 2, Population = 11.
20-
12-
2--
00-
0--
---
--0
0--
-0-

Generation = 3, Population = 14.
--0
2-0
-0-
110
1--
0-1
00-
--0
--0

Generation = 4, Population = 17.
201
-21
210
-21
---
-0-
-10
0-1
-01

Generation = 5, Population = 11.
31-
232
3-1
---
--0
01-
---
---
-1-

Generation = 6, Population = 14.
---
34-
412
1--
1--
12-
01-
-0-
1-1

Generation = 7, Population = 13.
31-
---
523
--1
---
2-1
-20
01-
-1-

Generation = 8, Population = 15.
421
--2
6--
1-2
1--
-22
0--
12-
-21

*** Life<FredkinCell> 8x9 ***

Generation = 0, Population = 42.
00-00----
00-00-000
00000-00-
0-0--0-00
00000----
-0---0--0
--0-0--00
0-000000-

Generation = 1, Population = 39.
-----0000
11011--11
1111-0-10
---00-0-1
-11-10-0-
-1000-0-1
0-1010---
----1----

*** Life<FredkinCell> 10x8 ***

Generation = 0, Population = 39.
--000-00
-00-00-0
-00-----
0----00-
00-0000-
--00-0--
-0000--0
-----0--
-0-0-0--
00-0-000

Generation = 1, Population = 42.
-----01-
0-10-101
---00-00
1000---0
-10----0
00-1-1-0
0111100-
--0--100
-1-1---0
1--1---1

Generation = 2, Population = 44.
0-000---
--2-0---
-00110-1
211-----
02-0-00-
-1-20201
-222---0
0010--1-
--0200--
--020-02

Generation = 3, Population = 37.
-----0--
0------1
----21--
---00-00
-3010---
--03-31-
-333-001
11-1----
0-1-1100
-0-3-013

*** Life<FredkinCell> 5x1 ***

Generation = 0, Population = 2.
-
-
-
0
0

Generation = 1, Population = 3.
-
-
0
1
1

Generation = 2, Population = 3.
-
0
1
-
2

Generation = 3, Population = 3.
0
1
2
-
-

Generation = 4, Population = 3.
1
-
3
1
-

Generation = 5, Population = 3.
-
-
4
2
2

Generation = 6, Population = 3.
-
1
5
-
3

*** Life<FredkinCell> 5x1 ***

Generation = 0, Population = 3.
-
0
0
0
-

Generation = 1, Population = 4.
0
1
-
1
0

Generation = 2, Population = 4.
1
2
-
2
1

Generation = 3, Population = 4.
2
3
-
3
2

Generation = 4, Population = 4.
3
4
-
4
3

Generation = 5, Population = 4.
4
5
-
5
4

*** Life<FredkinCell> 4x1 ***

Generation = 0, Population = 2.
-
0
-
0

Generation = 1, Population = 1.
0
-
-
-

Generation = 2, Population = 1.
-
0
-
-

Generation = 3, Population = 2.
0
-
0
-

Generation = 4, Population = 1.
-
-
-
0

Generation = 5, Population = 1.
-
-
0
-

Generation = 6, Population = 2.
-
0
-
0

Generation = 7, Population = 1.
0
-
-
-

Generation = 8, Population = 1.
-
0
-
-

Generation = 9, Population = 2.
0
-
0
-

Generation = 10, Population = 1.
-
-
-
0

*** Life<FredkinCell> 3x3 ***

Generation = 0, Population = 2.
---
0-0
---

Generation = 1, Population = 4.
0-0
---
0-0

Generation = 2, Population = 0.
---
---
---

Generation = 3, Population = 0.
---
---
---

Generation = 4, Population = 0.
---
---
---

Generation = 5, Population = 0.
---
---
---

Generation = 6, Population = 0.
---
---
---

Generation = 7, Population = 0.
---
---
---

Generation = 8, Population = 0.
---
---
---

Generation = 9, Population = 0.
---
---
---

Generation = 10, Population = 0.
---
---
---

*** Life<FredkinCell> 6x4 ***

Generation = 0, Population = 14.
00-0
--0-
-0-0
0-00
---0
0000

Generation = 1, Population = 13.
110-
00-0
---1
-011
-00-
1---

Generation = 2, Population = 8.
-21-
--01
0---
0-2-
----
--0-

Generation = 3, Population = 12.
13--
0--2
10-1
1--1
0---
-0-0

Generation = 4, Population = 11.
-410
-003
21--
--22
1---
----

Generation = 5, Population = 12.
1-2-
-11-
3-0-
--33
-000
1---

Generation = 6, Population = 14.
-430
0223
-1--
1-4-
-11-
--00

Generation = 7, Population = 12.
--4-
1-3-
3201
--53
-22-
---1

Generation = 8, Population = 13.
1450
22--
----
106-
13-0
-0--

Generation = 9, Population = 11.
-5-1
---3
--0-
-17-
-42-
-101

Generation = 10, Population = 14.
1--2
-2-4
--1-
1-83
153-
1-12

*** Life<FredkinCell> 10x6 ***

Generation = 0, Population = 29.
0-0---
-0----
-000--
000--0
0-0-0-
-00-00
-0--00
--0-00
-0-00-
-0-0-0

Generation = 1, Population = 33.
-0-0--
-100--
-11100
-11---
1---10
----1-
010011
-0-01-
010--0
01-10-

Generation = 2, Population = 32.
01010-
0211-0
02-2-1
------
--00-1
-00--0
12-1-2
0----0
--100-
--021-

Generation = 3, Population = 29.
-2121-
1-2201
--13-2
011---
---1-2
---01-
--0---
1-0--1
01-1--
-1-3-0

Generation = 4, Population = 30.
-323--
-23-1-
--2-03
1--0--
10-21-
----2-
121-12
201-1-
--120-
-2-4--

Generation = 5, Population = 31.
0-34--
1-422-
0-33-4
2--100
-1---2
--0---
-3--23
-120--
01-3-0
0-05--

Generation = 6, Population = 23.
1-----
--5-3-
---4-5
3----1
---2-3
-0-02-
141-34
-2----
------
121-10

Generation = 7, Population = 35.
-33-1-
12-2--
0---06
-1--0-
110314
-10--0
2-21-5
-3--11
0-1-00
2-2-21

Generation = 8, Population = 30.
--44-0
--5-31
12---7
32---1
2-1-2-
0-10--
---23-
2-2--2
112-11
3-3---

*** Life<FredkinCell> 10x3 ***

Generation = 0, Population = 10.
---
--0
--0
--0
0--
0--
---
00-
-0-
0-0

Generation = 1, Population = 18.
--0
-01
-0-
001
100
10-
-0-
1-0
01-
-0-

*** Life<FredkinCell> 10x1 ***

Generation = 0, Population = 6.
-
0
0
0
0
0
-
0
-
-

Generation = 1, Population = 4.
0
1
-
-
-
1
-
-
0
-

Generation = 2, Population = 7.
1
2
0
-
0
-
0
0
-
0

Generation = 3, Population = 4.
2
-
1
-
-
-
1
1
-
-

Generation = 4, Population = 5.
-
-
-
0
-
1
2
2
0
-

Generation = 5, Population = 4.
-
-
1
-
-
2
-
-
1
0

Generation = 6, Population = 7.
-
2
-
0
0
-
2
2
2
1

*** Life<FredkinCell> 1x7 ***

Generation = 0, Population = 5.
000-00-

Generation = 1, Population = 5.
1-1-110

*** Life<FredkinCell> 8x8 ***

Generation = 0, Population = 31.
-0000-00
--0---00
--0-----
0000-00-
0-0--000
-000---0
000----0
-0-0----

Generation = 1, Population = 26.
011-1---
----00--
0----0-0
---1----
1--00-1-
0--100--
1--0--01
-10-0--0

Generation = 2, Population = 38.
1-2-2---
-000--00
-0-0-1--
--02-000
2001-0-0
-0021100
2--10-1-
-210-0-1

Generation = 3, Population = 34.
-130-000
--1---11
01---20-
00-3-1--
31-20---
0-1-221-
-002--21
-3--0-0-

Generation = 4, Population = 32.
1241-11-
002--02-
---00---
11-402--
4-0--01-
1-2-----
--130-32
04----1-

Generation = 5, Population = 28.
-352---0
-1-0---1
-----20-
-20-1---
-11---20
2---22-0
----1--3
15-0002-

*** Life<FredkinCell> 4x5 ***

Generation = 0, Population = 8.
--0--
000--
-00--
---00

Generation = 1, Population = 11.
0-10-
1110-
----0
-0-11

Generation = 2, Population = 10.
10--0
--2--
0-001
0--2-

Generation = 3, Population = 10.
21-0-
--3--
1--12
10-3-

Generation = 4, Population = 10.
321-0
---00
2---3
-1-4-

Generation = 5, Population = 9.
4-201
-1-11
---14
-----

Generation = 6, Population = 10.
-231-
--322
-00--
---41

Generation = 7, Population = 13.
434--
-1433
21-1-
-1-52

Generation = 8, Population = 14.
54--1
12544
320-4
-2--3

Generation = 9, Population = 8.
----2
2-6-5
---1-
-3-54

Generation = 10, Population = 12.
5-413
---46
32-24
1---5

*** Life<FredkinCell> 8x10 ***

Generation = 0, Population = 41.
0-0-------
0-0000000-
-0-0-0-00-
000--00-0-
0----000-0
000-0--00-
00-0--0--0
0-00----0-

Generation = 1, Population = 43.
1-1-00000-
10-1-1-1-0
01-10---10
-11-01--1-
-0----1---
1110--0-10
1--1-0--0-
101-0-00--

Generation = 2, Population = 44.
20-011-11-
2-02-202-1
-20--0----
-220-2-02-
--000-20-0
-2--0-1--1
-0020-0-1-
--20-011--

*** Life<FredkinCell> 7x3 ***

Generation = 0, Population = 12.
0--
000
--0
00-
0-0
-00
--0

Generation = 1, Population = 11.
1-0
---
-01
-10
1-1
-11
--1

Generation = 2, Population = 7.
---
00-
0--
--1
---
-22
--2

Generation = 3, Population = 12.
10-
-10
1-1
01-
-0-
03-
--3

Generation = 4, Population = 8.
2--
0--
2-2
---
1-1
1--
0--

Generation = 5, Population = 10.
30-
-10
3--
---
2--
-32
10-

Generation = 6, Population = 8.
4--
0-1
--2
---
--1
1-3
2--

Generation = 7, Population = 16.
500
1-2
303
---
202
2-4
303

Generation = 8, Population = 0.
---
---
---
---
---
---
---
