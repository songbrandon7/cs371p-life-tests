*** Life<Cell> 14x28 ***

Generation = 0, Population = 10.
----------0--0--------------
----------------------------
----------------------------
----------------------------
-------------0--------------
----------------------------
-0------------------0-------
------------------0------0--
-----------------------0----
-------------0--------------
-----------0----------------
----------------------------
----------------------------
----------------------------

Generation = 7, Population = 181.
0-00--1---.11.10*10110---0--
-0-00--0-0-0-1-00-1-0-000-0-
--0-00----1-*-*1-0*0001-00-0
---0-000--0-01--100-*--*-00-
----0--1*1.--*--*0-11---*-0-
-----0--0--*-.-1--*00--0-101
------0-0----.----.-1-.0-01*
-----0--0--01--0-1-0....-10*
----0--100--010----10.-1-*0.
---0--0*01-.-01*-1-----0-*-*
--0---000*0--.01--*-----000-
-0-0-0-0010-0-0-1---*--*0001
0-0---0-00-0--01-000-1000*00
-0-----0--0----100-000--010-

Generation = 14, Population = 154.
***-..*..-.1*.*..---...--*00
--*0-001*0*-..-.**1-1--*1-01
-.1-01--*-*..-*.0**-*1..00-1
-11--111101.-**.*-1-.-1.--.*
0---1*-*..**.*-.*-*-.-**.-11
---1----*0***..-1*...--...-*
-00--*1--*.*-.1011.-.-*.....
-1--0-1..-*.1*-**.-...*.-.1*
-1-1-*.*-*-.**-1.10-..1*-.-.
---.-01.1.-.--..*.-..---..1.
----0*1-1.-1....-....0------
-0-----00.-.1.11.-.**10.00-.
-------1--0---*.11--**-...0-
---0-0-0--1-10-*010--*1...-1

*** Life<Cell> 15x8 ***

Generation = 0, Population = 10.
-0---0--
--------
-------0
--------
--------
--------
-0------
-------0
-----0--
---0----
--------
------0-
--------
0-------
-------0

Generation = 5, Population = 48.
----0-0-
---0-0--
--0-0--0
-0-0--0-
0-00---1
--0-00-1
0-0-0*0-
--0--*11
-.0.-0-*
-1----0-
--------
---1--1*
0--0--01
--1*10-0
0--10-0-

Generation = 10, Population = 49.
---*---.
-1---00*
-----011
0-----0*
-101-**.
1*0..**.
--*--.-.
*1.1-**1
*.....-*
-.-0..--
-1-.-.1*
-0*.-*1*
-10--0-*
0-..*--*
----0*-0

Generation = 15, Population = 47.
01*.**0*
-1-*-1.*
0.-1.0-.
00-11.**
.1*.-...
*.*....*
-..--...
*...-...
.*..*...
-..-..*-
1*-.-.**
01..*.*.
--1*0**.
11...-.*
0...1.-.

Generation = 20, Population = 29.
-.**..-.
.*1.*-..
*.*-.*-.
-0.-..*.
.1...**.
..**....
-..1-...
....0...
......*.
...1*.*-
1*.*.*.*
*....**.
-...-...
-.......
....-...

*** Life<Cell> 27x5 ***

Generation = 0, Population = 24.
0-0--
-----
---0-
--0--
----0
-00--
---00
0----
-----
--00-
-----
-----
-----
-----
-00-0
-----
---00
-----
-----
-----
-----
-----
-0---
0--00
--0--
-----
0-0--

*** Life<Cell> 12x28 ***

Generation = 0, Population = 22.
----------------0-----------
--------0-0----------00-----
------------0---------------
-------------0------0-------
----------------------------
-------------0--------------
-----00---------------------
---------0----------0-------
--------------0----0--------
0-0------------0-------0----
-----0----------0-----------
---------0------------------

Generation = 15, Population = 143.
--1..**-...1-..01-.1.-.-.00.
110--*-.-***.*.***.**.*.*.*-
0-.*.*.-.1*.*.*..1.-*-.-..**
--**.1.-****..*0*.*...-..1--
.....-.11..**-1--..*1.*-*.1.
**.**0*..........*1.*...**.-
1*****....-1*..1.*10-.*.0.-.
..-..*.*..*01*..****-.-..*-.
0-...--*-*.1*.*..*1-1.-*.**-
0-.1**.*...-0-...---**.*.--.
*.--*...**-..*-.01.*.0*.1-**
*.*.-*1.0*-.*1-1***-.1.0---0

*** Life<Cell> 15x26 ***

Generation = 0, Population = 12.
--------------------------
-------0------------------
--------------------------
-0------------------------
------------0-------------
-------0------------------
--------------------------
-----------0--------------
--0------------0----------
--------------------------
-------------------00-----
----------------------0---
--------------------------
--------------------------
-----------0----------0---

*** Life<Cell> 16x5 ***

Generation = 0, Population = 20.
----0
0--0-
---0-
0-00-
00--0
-00-0
--0--
--0--
---0-
-----
---0-
-----
-0---
----0
-----
--0--

Generation = 19, Population = 29.
..**.
1*..*
.**.*
..-*.
.....
.....
..-..
.*..*
.-***
-.*--
***.*
.****
.-1..
...1.
-1**1
-....

*** Life<Cell> 6x10 ***

Generation = 0, Population = 14.
-0--------
-0-0-0---0
-------0--
---0---0--
--0-------
-000---0-0

*** Life<Cell> 14x30 ***

Generation = 0, Population = 16.
-------------0----------------
--------------00--------------
-----------0----------0-------
------------------------------
------------------------------
--------0---------------------
---0--------------------------
--0-----------0----------0----
------------------------------
---0-0---0--------------------
----------------------------0-
------------------------------
------------------------------
-------0---0------------------

Generation = 7, Population = 169.
----0-0-000---0-0*---0---0-0--
-----0--1-.1-0.1.11-*10-0---0-
---0-00----0--1.*01-10-0-----0
--01--001--1-*.*---1--0-----0-
-00-0---000--1*------------0--
11-.-.0---01---.*--0------0---
--0-0.0-0---0-00-00-1----0----
1---00---111-1--100*-0--0-----
-00*0-01-00-0---0-000--0---0--
-**--00*-.-1-*-100-00-0---0-0-
0*---*0-00-0000-0-0------0-0-0
1-1-1--1*0-0-00*10--------0---
00-1-0-000-00-0-00-----------0
-01-0---0---00*1--0-------0-0-

*** Life<Cell> 12x26 ***

Generation = 0, Population = 25.
--------------------------
-0-----------------------0
0-------------------0---00
--0----0-----0-00-0-------
----0--------------0---0--
--------------------------
-------------------0------
0-------------------------
---0----------------------
0------0------0-----0-----
-----0-----0--------------
0-------------------------

Generation = 9, Population = 132.
--0-0*10-0----01-10*1*-0.-
0-*1*-.*0-0--0----*1*.1-.*
*1.-.-1-.-0---0*-...1-.**.
.*0.*.10*-----...--..--.-.
*-...**-0-0--1-.-*-.-0-...
.1.0*1*1-----1*--00-.100--
-0---*------0--.0**1.*0.1-
**0-.**.--10-1-.-1--------
*0.--***01-1--------.-0-00
-..*-*--**.0*-0-----00----
--10**1...0**-0-0-00-000--
0-*0--**-.0*11-*1--00---00

Generation = 18, Population = 100.
1......-..-*-.**-...**.-.-
.-.1*-..-**.--...1*.......
.*.*.-...*.*-.-*1.*.*-....
..-....-.*-*.-***.*..***..
*-..**.*.1-1-.**..*...-.*.
*.*..*..-**-....-........-
.*0.**.....0.....*..*....-
*.*1**...-**1.-...1.*.--.*
*.**.....*.--1*****.*11.*.
....-.-....*..**-**.*-...1
.-**......-*.0.1..*1***.-0
.1*---..-.0...-..1-.11--.-

Generation = 27, Population = 62.
-.*.*..-..1...........*-.-
.......**.....*.**.....*..
*****-**......*.*******...
*.....**...*......****....
........**-.-.....**..-...
...........-....-*......**
........................*-
.........-.***-...........
...........**.*...........
...*..-...*...*.-....1....
....***..**...*....-....-.
...1.*..-.*..*1..--.-.....

*** Life<Cell> 30x29 ***

Generation = 0, Population = 15.
-----------0-----------------
-----------------------------
---------------0-------------
-----------------------------
--0--------------------------
-----------------0-----------
-----------------------------
-----------------------------
0----0-----------------------
-----------------------------
---0-------------------------
-----------------------------
-----------------------------
-----------------------------
-----------------------------
-----------------------------
-------0---------------------
-----------------------------
0----------------------------
------------0------0---------
-----------------------------
----------------------0------
0---0------------------------
-----------------------------
-----------------------------
----------0------------------
-----------------------------
-----------------------------
-----------------------------
-----------------------------

*** Life<Cell> 15x14 ***

Generation = 0, Population = 11.
---0----------
--------------
--------------
-----0--------
0-------------
--------------
--------0--0--
0----0--------
------0-------
--------------
0-------------
--------------
------------0-
--------------
-------------0

Generation = 12, Population = 98.
*0*.-.--1-110-
---11.-01*.0--
...*..*100*-00
*--.0-100*1*01
*.*0*.-.*-----
*-**--..-1-*.*
*-*..-1......*
-*.-**.1-1.0.-
*..-1--1--.-**
*0-0*-.-0.-001
*-00*-*0.-0-.1
-1--01-*-00*0-
.-.-1--1.--**.
.0*0-..0-*0-.-
-00--.*1.-***.

Generation = 24, Population = 70.
..*.1.0**1*.*1
..*...-.....*1
....**.*-*..-.
..*..1*...*.**
.*..**..**1--.
.**.1*.*..1*..
........**...*
......*..-.***
......-.*.**.*
...-.....*...*
..*..-..*1.-*.
..-.1**.-....-
.-.0.*...*....
.*.*.....*****
1-.*0.*..-*...

*** Life<Cell> 22x14 ***

Generation = 0, Population = 9.
--------------
--------------
0-------0-----
------0-------
--------------
--------------
--------------
--------------
----------0---
--------------
--------------
-----------0--
--------------
--------------
-------0---00-
--------------
--------------
--------------
--------------
----0---------
--------------
--------------

*** Life<Cell> 5x13 ***

Generation = 0, Population = 20.
---00-0-----0
000----0---0-
-----------00
---0----000--
-0000----0---

*** Life<Cell> 18x21 ***

Generation = 0, Population = 25.
0-----------0--------
---------------------
---------------------
--0------------------
---------------------
-----------0---------
---------------------
--------------0-0----
---------0-----------
---------00---0------
------------------0--
---------------------
--00----------0-00---
---------0-----------
------0-0------------
--0------------------
---0-------0-0--0----
-------------0-------

*** Life<Cell> 5x7 ***

Generation = 0, Population = 17.
-0-00--
--00-0-
-0-0-0-
-00-000
-0---00

*** Life<Cell> 11x26 ***

Generation = 0, Population = 15.
----------0-------------0-
--------------------------
--------------00----------
--------------------------
--0--------------0--------
-------------0------------
--------------------------
---0--------0-------------
---------0------------0---
---0-------0-0------------
----------0---------------

Generation = 26, Population = 88.
-.1*.......*....0******.*.
.0...**-**.*.....0...*.-.-
.******..1.........*.1-...
..*-.....*.....-....*1**..
...*..*..*...*...**.**..*.
1.......*....*...**.......
-..*....*..--*...**..**-..
..........*******-..**-***
.******..*.......*.*....**
.*...*-...1..*.......*....
.-.-........*.....**...**.

*** Life<Cell> 10x24 ***

Generation = 0, Population = 9.
0----0------------0-----
------------------------
0--0--------------------
------------------------
----0-------------------
------------------------
----0-------------------
------------------------
------------------------
-0-------------------0--

Generation = 13, Population = 94.
--**.-.-.0--**100-10----
-.-*1--0--00-000010-0---
.****.-1.*.0-*--00-0---0
-1-.**----0-0--0--0---0-
-...-*.--0.11100-0------
0.**-10--1-001--------0-
1-1-*.*.----00---0---0-0
01-.-*00--00--0-0-----0-
0*-**1-1*-00------------
-00.0.--00----0---------

*** Life<Cell> 6x25 ***

Generation = 0, Population = 6.
-----------------0-------
--0----------------------
--------------0----------
-----0------------------0
-------------------0-----
-------------------------

Generation = 25, Population = 35.
**.-.1-.*.-....*.........
*....*..-*.-..*.*....-.-.
*.*-....**-....-.......**
.*.*1.*.*.*....-.......*.
..*.....**0........-.1*..
...**.**.-...-*-....--.*.

*** Life<Cell> 27x25 ***

Generation = 0, Population = 8.
-------------------------
----------0-------0------
--------0----------------
-------------------------
-------------------------
-----------0-------------
--------0----------------
-------------------------
-------------------------
-------------------------
---------0---------------
-------------------------
-------------------------
-------------------------
-------------------------
-------------------------
-------------------------
-------------------------
-------------------------
-------------------------
-------------------------
-------------------------
-------------------------
-------------------------
-------------------------
--0----------------------
-----------0-------------

Generation = 19, Population = 240.
.***1***.1.***0*.-1.--0-*
*....-.....**...-.0*-1-**
*.*--.*..1.-*...*0--.1.0.
11-.*..**1...-*--*.-1-..-
*010..-*.*..-1.*...*--1-*
.**--....*....-..**..1*--
1--.-*-..*..0*.-.-.-1--1*
10..-10*.*..-0*-.*-0-*0.1
**.*-1-**..1***-*.-.----1
*.-.*-*.1-***.*.*-1*-1--0
*-.*.0..**.-..100.-------
0**-..**-.-.**-*----00---
*0.11-00.-*.-.01-11------
*01...-0.---.1--.1---0---
00..00-*.*1-*--*1-0-0----
-0---1*01***-*1100-------
--00--*---*.-*..-0--0----
--------.0-.*.0-10-0-0---
------.1----.---0-0-0----
------0--0*------0-0-----
-------1.--.1-----0------
------0-0-00--0----------
-----0-0----00----0------
---0--0-00-110---0-0---0-
--0-0--011-00-0-0-0-0-0-0
-0---0-000---0---0-0-----
0---------0-0-0-0-------0

*** Life<Cell> 10x8 ***

Generation = 0, Population = 8.
--------
0-------
--------
0-------
---0----
0-------
----0---
------0-
--------
0--0----

*** Life<Cell> 9x17 ***

Generation = 0, Population = 10.
-------------0---
-----------------
-----------------
-----------------
0--------0-------
----0---------0--
-----------------
0-0--------0-----
-----0------0----

*** Life<Cell> 5x11 ***

Generation = 0, Population = 24.
0-0-000--00
--00--0---0
-00--------
0--0--000-0
-000-0----0

Generation = 8, Population = 20.
***.*---0*-
--1..-..*1-
*.*-.-.-0*-
-.1..---*-.
.*0*---**-.

Generation = 16, Population = 31.
***.**1.-*-
....*****.*
**.1..*1..*
-**..1.1**.
..***-*.*1.

Generation = 24, Population = 17.
.*..***...-
.*.....*...
*..*.**....
-*...-.*...
.**.**.*.-.

*** Life<Cell> 27x29 ***

Generation = 0, Population = 29.
----------0------------------
--------------0--------------
--------------0--------0-----
-----0-----0---------0-------
------0----------------------
-----0-----------------------
-0--------0------0-----------
-----------------------------
---0-------------------------
--------0--------------------
-----------0-----------------
-----------------------------
------0-----------------0----
-----------------------------
-----0-----------------0-----
----------------------------0
------------------------0----
--------------0--------------
-----------------------------
-----------------------------
-----------------------------
-------------------0---------
-----------------------------
-0----------------0----------
--------------------0-----0--
-----------------------------
-----------0--0--------------

Generation = 11, Population = 361.
0.1.---0--**--1*0*-.---0010-0
1.--*.*-*.*-*-**--.01--1-1---
1--00-*-*01-*1**..****-1-.---
0*-*0-0100-.0...1...--0.1.*.1
..**--.-.0*-11101..*01--*-1--
-.0*-.1..*1.*..-0----.-.0-0--
-*.-*-*0.*-...*-1-....-*.---0
*--1-.-1---..**-10010.-.1*-01
0*-.--*-0--.---000--****1*--*
--11--..1-**01*0--1-00-*----1
*-.--.*--110-110---.0*.*.--0.
-----.1------1--*00----**11-.
*..***--.-.-1*-1*.-.1-*.-0*-*
0--0-1--1-11--00-***--*-*--0*
-0--0-.11-0*--10-1----0---.1-
01-00-0-.0100-0---.-.*1.*-.-*
0.01*.**-1100---*-.0-*...1..1
0*0--*.0.01-00-1-*..*-*..--1*
-00-0-.*----10-0-0*-01-*-1*01
---10-0-1-0*--0-10.1*-1.-0--0
-0-01*10-1----*-0-.---00*0101
0-00-*-00--.-**...*--1----1--
-00-0.*1--011.--*-0.*-*-0*101
-----1---1.--11.1*0..-..--*1-
-00---110--1.*-1-1*1***-----*
010---0--0--0-*0-.-10---00---
01-1-*11--0.***--*---**--0001

*** Life<Cell> 17x14 ***

Generation = 0, Population = 12.
--------------
--------------
------0-------
-------------0
-------------0
--------------
--------------
--0-----------
-----0--------
--------------
--------------
------0-------
0------------0
--------------
-----0--------
0---0---------
------0-------

Generation = 14, Population = 113.
11.-1*1.1--...
--.10.1*--*-0.
.1---0**1..*..
*-.0*1..-.*.*.
--**0.....**..
-11----0-*-*..
**--**10-.**1.
*.--------.1*-
*.**---1..11.1
0.00.*--*10-11
**--*1.*-.*01*
11-*.*10.-0*--
..-.****.-*01*
-*..*0**.*11-*
..*---*....-.-
*..-.*.-*-*-*1
.-*****-*.**.1

*** Life<Cell> 22x12 ***

Generation = 0, Population = 19.
----------0-
------------
--0-0-------
------------
--0---------
------0-----
------------
------------
0-----------
----------0-
-0----------
----0-------
-0----------
---------0--
-0----------
-0----0-----
-0------0---
------------
------------
------------
-0--0-------
--------0---

Generation = 11, Population = 122.
110**-.-*-*0
--*-.-*1.-.0
*-1-*...0.1*
1010-*-1*1--
-..0-*10--10
..11--*-*11-
*.-..---1-*0
1-10.**0---0
.-*1*.1-0-*0
1.10.-1--..-
.*-1.1**.*--
*..-1**-..*-
*-*1*.*11-.-
.10-.-.**-..
**-.-.1..-0-
1*........-.
**.....***0-
-.*-.....-*0
*..*-*1*.0*0
*-*--0.-.1.0
*-*.*****-1-
*--.***--.--

Generation = 22, Population = 81.
*....1*-..*.
*..-*.**..*.
*..........*
.*.-.*-*.**0
0..-1..*.*..
.*.*-*.**.*-
**..........
*0.-.*..-.-.
*.*.*...-*.-
*.*..*..***.
..*.*-...*..
.......-.*..
...-.....*.-
...0*0***...
....-*...*1.
..**....**..
...*.......*
-..-..*.....
........***.
***0*-*.*...
*-.*.*.*...1
*.*....*-...
